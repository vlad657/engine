# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
# Keep ARCore public-facing classes
-keepparameternames
-renamesourcefileattribute SourceFile
# These are part of the Java <-> native interfaces for ARCore.
-keepclasseswithmembernames,includedescriptorclasses class com.google.ar.** {
    native <methods>;
}
-keep public class com.google.ar.core.** {*;}
-keep class com.google.ar.core.annotations.UsedByNative
-keep @com.google.ar.core.annotations.UsedByNative class *
-keepclassmembers class * {
    @com.google.ar.core.annotations.UsedByNative *;
}
-keep class com.google.ar.core.annotations.UsedByReflection
-keep @com.google.ar.core.annotations.UsedByReflection class *
-keepclassmembers class * {
    @com.google.ar.core.annotations.UsedByReflection *;
}
# Keep Dynamite classes
# .aidl file will be proguarded, we should keep all Aidls.
-keep class com.google.vr.dynamite.client.IObjectWrapper { *; }
-keep class com.google.vr.dynamite.client.ILoadedInstanceCreator { *; }
-keep class com.google.vr.dynamite.client.INativeLibraryLoader { *; }
# Keep annotation files and the file got annotated.
-keep class com.google.vr.dynamite.client.UsedByNative
-keep @com.google.vr.dynamite.client.UsedByNative class *
-keepclassmembers class * {
    @com.google.vr.dynamite.client.UsedByNative *;
}
-keep class com.google.vr.dynamite.client.UsedByReflection
-keep class com.google.ar.sceneform.ArSceneView
-keep @com.google.vr.dynamite.client.UsedByReflection class *
-keep class com.google.ar.sceneform.assets.Loader
-keepclassmembers class * {
    @com.google.vr.dynamite.client.UsedByReflection *;
}
-dontwarn com.google.ar.sceneform.animation.AnimationEngine
-dontwarn com.google.ar.sceneform.animation.AnimationLibraryLoader
-dontwarn com.google.devtools.build.android.desugar.runtime.ThrowableExtension
-keep class com.google.android.gms.location.LocationRequest