/*
    The Great Patriotic War
    Copyright © Vladislav Rozhkov 2019

 */

package com.vladd11.ar.engineapp;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.ar.core.ArCoreApk;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.vladd11.ar.engineapp.ui.HomeFragment;
import com.vladd11.ar.engineapp.ui.InfoFragment;
import com.vladd11.ar.engineapp.ui.PreviewFragment;
import com.vladd11.ar.engineapp.ui.SetttingsFragment;

/**
 * Main app activity
 */
public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";
    public static final double NEEDED_GL_VERSION = 0x30000;
    public static double device_gl_version = 0;
    @Nullable
    public static FirebaseAnalytics mAnalytics = null;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("analytics", true)) {
            mAnalytics = FirebaseAnalytics.getInstance(this);
        }

        final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo;
        if (activityManager != null) {
            configurationInfo = activityManager.getDeviceConfigurationInfo();
            device_gl_version = Double.parseDouble(configurationInfo.getGlEsVersion());
        }

        final BottomNavigationView navigationView = findViewById(R.id.navigationView);

        navigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.home:
                    if (mAnalytics != null) {
                        mAnalytics.logEvent("home", new Bundle());
                    }
                    openFragment(new HomeFragment());
                    return true;

                case R.id.settings:
                    if (mAnalytics != null) {
                        mAnalytics.logEvent("settings", new Bundle());
                    }
                    openFragment(new SetttingsFragment());
                    return true;

                case R.id.info:
                    if (mAnalytics != null) {
                        mAnalytics.logEvent("info", new Bundle());
                    }
                    openFragment(new InfoFragment());
                    return true;
                case R.id.preview:
                    if (device_gl_version > NEEDED_GL_VERSION) {
                        if (mAnalytics != null) {
                            final Bundle bundle = new Bundle();
                            bundle.putDouble("glVersion", device_gl_version);
                            bundle.putDouble("androidVersion", Build.VERSION.SDK_INT);
                            bundle.putString("phone_model", Build.MODEL);

                            mAnalytics.logEvent("not_supported", bundle);
                        }

                        Toast.makeText(this, R.string.unsupported, Toast.LENGTH_LONG).show();
                        navigationView.setSelectedItemId(0);
                        return true;
                    }
                    if (mAnalytics != null) {
                        mAnalytics.logEvent("preview", new Bundle());
                    }
                    openFragment(new PreviewFragment());
                    return true;
            }
            return false;
        });

        openDefaultFragment(navigationView);
    }

    public void openDefaultFragment(BottomNavigationView navigationView) {
        final ArCoreApk.Availability availability = ArCoreApk.getInstance().checkAvailability(this);
        if (availability.isTransient()) {
            new Handler().postDelayed(() -> openDefaultFragment(navigationView), 200);
        }
        if (availability.isSupported()) {
            if (checkIsSceneformSupported()) {
                openFragment(new HomeFragment());
                navigationView.setSelectedItemId(R.id.home);
            } else {
                openFragment(new InfoFragment());
                navigationView.setSelectedItemId(R.id.info_view);
            }
        } else { // The device is unsupported or unknown.
            if (checkIsSceneformSupported()) {
                openFragment(new PreviewFragment());
                navigationView.setSelectedItemId(R.id.preview);
            } else {
                openFragment(new InfoFragment());
                navigationView.setSelectedItemId(R.id.info_view);
            }
        }
    }

    public boolean checkIsSceneformSupported() {
        final String openGlVersionString =
                ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        return Double.parseDouble(openGlVersionString) >= 3.0;
    }

    /**
     * Method to open needed fragment
     *
     * @param fragment Fragment to open
     */
    public void openFragment(Fragment fragment) {
        if (fragment == null) throw new NullPointerException("fragment variable is null");

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }
}
