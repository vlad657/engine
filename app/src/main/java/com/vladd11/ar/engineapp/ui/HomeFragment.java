package com.vladd11.ar.engineapp.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.ar.core.ArCoreApk;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.vladd11.ar.engineapp.GameActivity;
import com.vladd11.ar.engineapp.MainActivity;
import com.vladd11.ar.engineapp.R;

public class HomeFragment extends Fragment {
    private FirebaseAnalytics analytics;
    private Button play_arcore;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity activity = getActivity();
        if (activity == null) throw new NullPointerException("activity variable is null");

        play_arcore = activity.findViewById(R.id.play_plane);

        if (MainActivity.device_gl_version >= MainActivity.NEEDED_GL_VERSION || Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            checkSupported(activity);
        }

        play_arcore.setOnClickListener(view -> {
            if (MainActivity.mAnalytics != null) {
                MainActivity.mAnalytics.logEvent("play_arcore", new Bundle());
            }
            Intent intent = new Intent(activity, GameActivity.class);
            activity.startActivity(intent);
        });
    }

    // From Sceneform
    public void checkSupported(Context context) {
        if (context == null) throw new NullPointerException("context variable is null");

        final ArCoreApk.Availability availability = ArCoreApk.getInstance().checkAvailability(context);
        if (availability.isTransient()) {
            new Handler().postDelayed(() -> checkSupported(context), 200);
        }
        if (availability.isSupported()) {
            play_arcore.setEnabled(true);
        } else {
            if (MainActivity.mAnalytics != null) {
                final Bundle bundle = new Bundle();
                bundle.putString("phone_model", Build.MODEL);
                MainActivity.mAnalytics.logEvent("not_supported", bundle);
            }
        }
    }
}
