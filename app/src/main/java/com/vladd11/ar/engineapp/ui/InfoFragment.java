package com.vladd11.ar.engineapp.ui;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.vladd11.ar.engineapp.R;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

public class InfoFragment extends Fragment {
    public static InfoFragment newInstance() {
        return new InfoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.info_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final AssetManager assetManager = getActivity().getAssets();

        String path = "info_" + Locale.getDefault().getLanguage() + ".html";

        try {
            if (!Arrays.asList(Objects.requireNonNull(assetManager.list(""))).contains(path)) {
                path = "info_en.html";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        WebView infoWebView = getActivity().findViewById(R.id.info_view);
        infoWebView.getSettings().setJavaScriptEnabled(false);
        infoWebView.loadUrl("file:///android_asset/" + path);
    }

}
