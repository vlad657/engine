package com.vladd11.ar.engineapp.ui;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.filament.gltfio.Animator;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.SceneView;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.vladd11.ar.engine.Model;
import com.vladd11.ar.engineapp.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.concurrent.TimeUnit.SECONDS;

public class PreviewFragment extends Fragment {
    private final Set<Animator> animatiors = new HashSet<>();

    private final List<Node> nodes = new ArrayList<>();
    @NonNull
    private final List<Model> models = new ArrayList<>();
    private final Handler frameHandler = new Handler();
    private boolean lock = false;
    private int x = 0;
    private int y = 0;
    private int z = 0;
    private Node current_node = null;
    private SceneView sceneView;
    private Activity activity;
    private boolean isAnimationPlay = false;
    private FrameRunnable frameRunnable;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.preview_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();

        {% for i in range(models|length) %}
            models.add(new Model({{ i }}, "{{ models[i].name }}", R.raw.{{ extract(models[i]) }}));
        {% endfor %}

        sceneView = activity.findViewById(R.id.sceneView);

        sceneView.getScene().getCamera().setLocalPosition(new Vector3(0.1f, 1f, 1.2f));
        sceneView.getScene().getCamera().setLocalRotation(Quaternion.eulerAngles(new Vector3(335, 360, 159)));

        sceneView.doFrame(1);

        final SeekBar joystickView = activity.findViewById(R.id.x);

        joystickView.setMax(360);
        joystickView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (!b || current_node == null) return;

                x = i;
                current_node.setWorldRotation(
                        Quaternion.eulerAngles(new Vector3(i, y, z)));

                if (!isAnimationPlay) {
                    sceneView.doFrame(1);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        final SeekBar joystickView1 = activity.findViewById(R.id.y);

        joystickView1.setMax(360);
        joystickView1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (!b || current_node == null) return;

                y = i;
                current_node.setWorldRotation(
                        Quaternion.eulerAngles(new Vector3(x, i, z)));

                if (!isAnimationPlay) {
                    sceneView.doFrame(1);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        final SeekBar joystickView2 = activity.findViewById(R.id.z);

        joystickView2.setMax(360);
        joystickView2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (!b || current_node == null) return;

                z = i;
                current_node.setWorldRotation(
                        Quaternion.eulerAngles(new Vector3(x, y, i)));

                if (!isAnimationPlay) {
                    sceneView.doFrame(1);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        final Button switch_button = activity.findViewById(R.id.switch_model);
        switch_button.setOnClickListener((button) -> {
            if (current_node == null) return;

            current_node.setEnabled(false);

            int new_node = Integer.parseInt(current_node.getName()) + 1;
            if (new_node == models.size()) {
                new_node = 0;
            }

            current_node = nodes.get(new_node);
            current_node.setEnabled(true);

            for (Model model :
                    models) {
                    /*if (model.getPlayer() != null && model.getPlayer().isPlaying()) {
                        model.stop();
                    }*/
            }

            if (!isAnimationPlay) {
                sceneView.doFrame(1);
            }
        });

        final Handler handler = new Handler();
        handler.post(new FrameRunnable(animatiors, sceneView));
        for (Model model : models) {
            nodes.add(null); // Fix race condition
            ModelRenderable.builder().setIsFilamentGltf(true).setSource(activity, model.getResId()).build()
                    .thenAccept((modelRenderable) -> {
                        final Node node = new Node();
                        node.setParent(sceneView.getScene());
                        node.setLocalPosition(new Vector3(0f, 0f, 0f));
                        node.setWorldRotation(Quaternion.eulerAngles(new Vector3(90, 0, 0)));
                        node.setLocalScale(new Vector3(1, 1, 1));
                        node.setName(String.valueOf(model.getId()));
                        node.setRenderable(modelRenderable);

                        final Animator animator = node.getRenderableInstance().getFilamentAsset().getAnimator();
                        if (animator.getAnimationCount() > 0) {
                            animatiors.add(animator);
                        }

                        if (Integer.parseInt(node.getName()) == 0) {
                            node.setEnabled(true);
                            current_node = node;

                            joystickView.setProgress(90);
                            joystickView1.setProgress(0);
                            joystickView2.setProgress(0);
                        } else {
                            node.setEnabled(false);
                        }

                        sceneView.getScene().addChild(node);
                        nodes.set(model.getId(), node); // Fix race condition
                    }).exceptionally(throwable -> {
                throwable.printStackTrace();
                return null;
            });

            /*sceneView.setOnClickListener((view) -> {
                current_model++;
                if (current_model == models.size()) {
                    current_model = 0;
                }

                float x = current_model * 4;
                sceneView.getScene().getCamera().setWorldPosition(new Vector3(x, 0, 0));

                sceneView.doFrame(1);
            });*/
        }

        sceneView.doFrame(1);
    }

    public SceneView getSceneView() {
        return sceneView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        frameHandler.removeCallbacks(frameRunnable);
        frameRunnable = null;
        sceneView.destroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (isAnimationPlay) {
                frameRunnable = new FrameRunnable(animatiors, sceneView);
                frameHandler.postDelayed(frameRunnable, 41);
            }
            sceneView.resume();
        } catch (CameraNotAvailableException e) {
            e.printStackTrace();
        }
    }

    static class FrameRunnable implements Runnable {
        private final Collection<Animator> animators;
        private final SceneView sceneView;
        private final long startTime;

        public FrameRunnable(Collection<Animator> animators, SceneView sceneView) {
            this.animators = animators;
            this.sceneView = sceneView;
            this.startTime = System.nanoTime();
        }

        @Override
        public void run() {
            for (Animator animator :
                    animators) {
                animator.applyAnimation(
                        0,
                        (float) ((System.nanoTime() - startTime) / (double) SECONDS.toNanos(1))
                                % animator.getAnimationDuration(0));
            }
            sceneView.doFrame(1);
        }
    }
}
