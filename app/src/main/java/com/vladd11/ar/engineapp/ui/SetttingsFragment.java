package com.vladd11.ar.engineapp.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.vladd11.ar.engineapp.MainActivity;
import com.vladd11.ar.engineapp.R;

public class SetttingsFragment extends Fragment {
    public static SetttingsFragment newInstance() {
        return new SetttingsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.setttings_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final SharedPreferences.Editor editor = preferences.edit();

        final SwitchMaterial sound = getActivity().findViewById(R.id.sound);
        sound.setChecked(preferences.getBoolean("sound", false));

        sound.setOnCheckedChangeListener((compoundButton, checked) -> {
            if(MainActivity.mAnalytics != null) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("sound", checked);
                MainActivity.mAnalytics.logEvent("sound", bundle);
            }
            editor.putBoolean("sound", checked);
            editor.apply();
        });

        final SwitchMaterial analytics = getActivity().findViewById(R.id.analytics);
        analytics.setChecked(preferences.getBoolean("analytics", true));

        analytics.setOnCheckedChangeListener(((compoundButton, b) -> {
            editor.putBoolean("analytics", b);
            editor.apply();
        }));
    }

}
