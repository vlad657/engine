package com.vladd11.ar.engine;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.RawRes;
import androidx.fragment.app.FragmentManager;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import java.io.IOException;
import java.util.List;


public class Engine {
    private static final String TAG = "Engine";
    private final ArFragment arFragment;
    private final Renderable[] renderables;
    private boolean deleteMode;
    private final Activity activity;
    private MediaPlayer player;
    private int currentRes;

    public ArFragment getArFragment() {
        return arFragment;
    }

    public Renderable[] getRenderables() {
        return renderables;
    }

    public Engine(ViewGroup root, Activity activity, List<Model> models, FragmentManager fragmentManager) {
        this.activity = activity;
        renderables = new Renderable[models.size()];

        final LayoutInflater inflater = LayoutInflater.from(activity);
        final ViewGroup group = (ViewGroup) inflater.inflate(R.layout.view, root);
        final LinearLayout layout = group.findViewById(R.id.menu);

        for (Model model : models) {
            final Button button = (Button) inflater.inflate(R.layout.button, layout, false);
            button.setText(model.getName());
            layout.addView(button);
        }

        group.findViewById(R.id.delete).setOnClickListener(v -> deleteMode = !deleteMode);
        arFragment = (ArFragment) fragmentManager.findFragmentById(R.id.ux_fragment);

        models.forEach(model -> ModelRenderable.builder()
                .setSource(
                        activity,
                        model.getResId())
                .setIsFilamentGltf(true)
                .build()
                .thenAccept(
                        modelRenderable -> renderables[model.getId()] = modelRenderable)
                .exceptionally(
                        throwable -> {
                            activity.finish();
                            return null;
                        }));

        final Handler handler = new Handler();

        arFragment.setOnTapArPlaneListener(
                (HitResult hitResult, Plane plane, MotionEvent motionEvent) -> {
                    deleteMode = false;
                    layout.setVisibility(View.VISIBLE);
                    for (int i = 0; i < layout.getChildCount(); i++) {
                        final int finalI = i;
                        layout.getChildAt(i).setOnClickListener(v -> {
                            final Anchor anchor = hitResult.createAnchor();
                            final AnchorNode anchorNode = new AnchorNode(anchor);
                            anchorNode.setParent(arFragment.getArSceneView().getScene());

                            final TransformableNode node = new TransformableNode(arFragment.getTransformationSystem());
                            node.setParent(anchorNode);
                            node.setRenderable(renderables[finalI]);
                            node.setOnTapListener((hitTestResult, motionEvent1) -> {
                                final Model model = models.get(finalI);

                                if (deleteMode) anchorNode.removeChild(node);
                                else if (model.soundRes != -1) {
                                    try {
                                        setPlayerRes(model.getSoundRes());
                                    } catch (IOException ioException) {
                                        ioException.printStackTrace();
                                    }
                                }
                            });
                            node.select();
                            layout.setVisibility(View.GONE);
                        });
                    }
                    handler.postDelayed(() -> layout.setVisibility(View.GONE), 2000);
                });
    }

    public void setPlayerRes(@RawRes int res) throws IOException {
        if (res == currentRes) return;

        this.currentRes = res;
        if (player != null) player.stop();
        player = MediaPlayer.create(activity, res);
        player.start();
    }

    public void pause() {
        if(player != null) player.stop();
    }
}
