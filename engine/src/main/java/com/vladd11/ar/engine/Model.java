package com.vladd11.ar.engine;

import androidx.annotation.RawRes;

public class Model {
    private final int id;
    private final String name;
    @RawRes
    private final int resId;
    public final int soundRes;

    public Model(int id, String name, int resId) {
        this(id, name, resId, -1);
    }

    public Model(int id, String name, int resId, int soundRes) {
        this.id = id;
        this.name = name;
        this.resId = resId;
        this.soundRes = soundRes;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getResId() {
        return resId;
    }

    public int getSoundRes() {
        return soundRes;
    }
}
