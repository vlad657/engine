import os
import platform
import subprocess
import tkinter as tk
from pathlib import Path
from shutil import copy, copytree
from tkinter import filedialog

import jinja2


class Application(tk.Frame):
    models = []

    def __init__(self):
        self.root = tk.Tk()
        self.root.wm_title('Сборка')
        super().__init__(self.root)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        create_model = tk.Button(self)
        create_model["text"] = "Добавить модель"
        create_model["command"] = self.add_model
        create_model.grid(column=0, row=0)

        self.entry = tk.Text(self)
        self.entry.insert(1.0, '<h3>Справка</h3>')
        self.entry.grid(column=0, row=1)

        tk.Button(self, text='Собрать', command=self.build).grid(column=0, row=2)

    def add_model(self):
        model = Model()
        frame = tk.Toplevel(self)
        frame.wm_title('Модель')

        frame.protocol("WM_DELETE_WINDOW", lambda: self.root.destroy())
        model.root = frame

        tk.Label(frame, text='Модель').grid(column=0, row=0)

        path_edit = tk.Label(frame, text='Имя:')
        path_edit.grid(column=0, row=1)
        name_widget = tk.Entry(frame)
        name_widget.grid(column=1, row=1)
        model.name_widget = name_widget

        path_edit = tk.Label(frame, text='.GLTF файл:')
        path_edit.grid(column=0, row=2)
        browse_button = tk.Button(frame, command=model.open_dialog)
        browse_button['text'] = 'Просмотр'
        browse_button.grid(column=1, row=2)

        path_var = tk.StringVar(self)
        tk.Label(frame, textvariable=path_var).grid(column=2, row=2)
        model.text_var = path_var

        ok_button = tk.Button(frame, command=model.ok_button)
        ok_button['text'] = 'Сохранить'
        ok_button.grid(column=1, row=4)
        model.ok_button = ok_button

        self.models.append(model)

    templateLoader = jinja2.FileSystemLoader(searchpath="app/src/main")
    templateEnv = jinja2.Environment(loader=templateLoader)

    def build(self):
        Path(os.environ.get('USERPROFILE') + '\\AppData\\Local\\Android\\Sdk').mkdir(parents=True, exist_ok=True)

        template = self.templateEnv.get_template('java/com/vladd11/ar/engineapp/GameActivity.java.template')
        result = template.render(models=self.models, extract=extract)
        with open('app/src/main/java/com/vladd11/ar/engineapp/GameActivity.java', mode='w') as f:
            f.write(result)

        template = self.templateEnv.get_template('java/com/vladd11/ar/engineapp/ui/PreviewFragment.java.template')
        result = template.render(models=self.models, extract=extract)
        with open('app/src/main/java/com/vladd11/ar/engineapp/ui/PreviewFragment.java', mode='w') as f:
            f.write(result)

        with open('local.properties', mode='w') as f:
            userprofile = os.environ.get("USERPROFILE").replace("\\", "\\\\")
            f.write(f'sdk.dir={userprofile}\\\\AppData\\\\Local\\\\Android\\\\Sdk')

        # IMPORTANT: It thing accepts licenses
        # Don't run this if you aren't accept it.
        copytree('licenses', f'{os.environ.get("USERPROFILE")}\\AppData\\Local\\Android\\Sdk\\licenses')

        with open('app/src/main/assets/info_en.html', mode='w') as f:
            f.write(self.entry.get(1.0, tk.END))

        for model in self.models:
            copy(model.gltf, 'app/src/main/res/raw')

        if platform.system() == 'Windows':
            subprocess.check_call('gradlew.bat assembleDebug', shell=True)
            subprocess.call(f'explorer /select,"{os.path.abspath("app/build/outputs/apk/debug/app-debug.apk")}"')
        else:
            subprocess.check_call('./gradlew assembleDebug', shell=True)


def extract(string):
    return os.path.basename(string.gltf).split('.')[0]


class Model:
    def __init__(self, name_widget=None, gltf_widget=None, name=None, gltf=None, text_var=None,
                 ok_button_widget=None, root=None):
        self.root = root
        self.ok_button_widget = ok_button_widget
        self.text_var = text_var
        self.gltf = gltf
        self.gltf_widget = gltf_widget
        self.name = name
        self.name_widget = name_widget

    def open_dialog(self):
        self.gltf = filedialog.askopenfilename()
        self.text_var.set(self.gltf)

    def ok_button(self):
        self.name = str(self.name_widget.get())


if __name__ == '__main__':
    app = Application()
    app.mainloop()

# Full code in repository: https://bitbucket.org/vlad657/engine/src/master/
